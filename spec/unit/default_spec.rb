require 'spec_helper'

describe 'gitlab_elasticsearch::default' do
  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
        node.normal['gitlab_elasticsearch']['jvm_options'] = ['-XX:+HeapDumpOnOutOfMemoryError', '-XX:+UseParNewGC']
        node.normal['gitlab_elasticsearch']['lvm'] = true
        node.normal['gitlab_elasticsearch']['physical_volumes'] = ['/dev/sdy', '/dev/sdz']
      }.converge(described_recipe)
    end

    before do
      stub_command('/usr/share/elasticsearch/bin/elasticsearch-plugin list | grep prometheus-exporter').and_return(false)
      stub_command('/usr/share/elasticsearch/bin/elasticsearch-plugin list | grep x-pack').and_return(false)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates elasticsearch_user' do
      expect(chef_run).to create_elasticsearch_user('elasticsearch')
    end

    it 'installs elasticsearch' do
      expect(chef_run).to install_elasticsearch('elasticsearch').with_version('5.4.1')
    end

    # it 'locks elasticsearch package' do
    #  expect(chef_run).to lock_apt_package('elasticsearch')
    # end

    it 'installs java jre' do
      expect(chef_run).to install_apt_package('openjdk-8-jre')
      # expect(chef_run).to lock_apt_package('openjdk-8-jre')
    end

    it 'configures elasticsearch' do
      expect(chef_run).to manage_elasticsearch_configure('elasticsearch').with(
        configuration: {
          'cluster.name' => 'es_cluster',
          'node.name' => 'Fauxhai',
          'network.host' => '0.0.0.0',
          'node.data' => true,
          'node.ingest' => true,
          'node.master' => true,
          'discovery.zen.ping.unicast.hosts' => ['127.0.0.1', '[::1]'],
          'discovery.zen.minimum_master_nodes' => 1,
          'xpack.security.enabled' => false,
        },
        jvm_options: ['-XX:+HeapDumpOnOutOfMemoryError', '-XX:+UseParNewGC']
      )
    end

    it 'installs prometheus_exporter plugin' do
      expect(chef_run).to install_elasticsearch_plugin('prometheus_exporter').with(
        url: 'https://github.com/vvanholl/elasticsearch-prometheus-exporter/releases/download/5.4.1.0/elasticsearch-prometheus-exporter-5.4.1.0.zip'
      )
    end

    it 'installs x-pack plugin' do
      expect(chef_run).to install_elasticsearch_plugin('x-pack')
    end

    it 'sets up LVM PVs and VG' do
      expect(chef_run).to ChefSpec::Matchers::ResourceMatcher.new(:lvm_volume_group, :create, 'es_vg')
    end

    it 'sets up new permissions on ES mounted dir' do
      expect(chef_run).to create_directory('/var/lib/elasticsearch').with(
        user: 'elasticsearch',
        group: 'elasticsearch',
        mode: '0755'
      )
    end

    it 'starts elasticsearch service' do
      expect(chef_run).to configure_elasticsearch_service('elasticsearch')
    end
  end
end
